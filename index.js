/* Bài 1: Viết chương trình nhập vào ngày, tháng, năm (Giả sử nhập đúng, không cần kiểm tra hợp lệ).
Tìm ngày, tháng, năm của ngày tiếp theo. Tương tự tìm ngày tháng năm của ngày trước đó. */

// input: nhập ngày, tháng, năm (giả sử nhập đúng)
// output: trả lại ngày tháng năm hôm trước, hôm sau

document.getElementById('Tinh1').onclick = function () {

    var ngay = Number(document.getElementById('Ngay1').value);
    var thang = Number(document.getElementById('Thang1').value);
    var nam = Number(document.getElementById('Nam1').value);

    var ngaytruoc = 0;
    var ngaysau = 0;
    var thangtruoc = 0;
    var thangsau = 0;
    var namtruoc = 0;
    var namsau = 0;

    if (((nam % 4 == 0) && (thang == 2) && (ngay >= 30)) || ((thang == 2) && (ngay >= 29)) || ((ngay == 31) && (thang == 4, 6, 9, 11))) {
        alert('Ngày nhập không hợp lệ')
    } else if ((ngay == 1) && (thang == 1)) {
        ngaytruoc = 31;
        ngaysau = 2;
        thangtruoc = 12;
        thangsau = 1;
        namtruoc = nam - 1;
        namsau = nam;
    } else if ((ngay == 1) && (thang == 3)) {
        if (nam % 4 == 0) {
            ngaytruoc = 29;
            ngaysau = 2;
            thangtruoc = 2;
            thangsau = 3;
            namtruoc = nam;
            namsau = nam;
        } else {
            ngaytruoc = 28;
            ngaysau = 2;
            thangtruoc = 2;
            thangsau = 3;
            namtruoc = nam;
            namsau = nam;
        }
    } else if ((ngay == 1) && ((thang == 5) || (thang == 7) || (thang == 10) || (thang == 12))) {
        ngaytruoc = 30;
        ngaysau = 2;
        thangtruoc = thang - 1;
        thangsau = thang;
        namtruoc = nam;
        namsau = nam;
    } else if ((ngay == 1) && ((thang == 2) || (thang == 4) || (thang == 6) || (thang == 8) || (thang == 9) || (thang == 11))) {
        ngaytruoc = 31;
        ngaysau = 2;
        thangtruoc = thang - 1;
        thangsau = thang;
        namtruoc = nam;
        namsau = nam;
    } else if (ngay == 31) {
        if (thang == 12) {
            ngaysau = 1;
            ngaytruoc = 30;
            thangsau = 1;
            thangtruoc = thang;
            namsau = nam + 1;
            namtruoc = nam;
        } if (thang == 1, 3, 5, 7, 8, 10) {
            ngaysau = 1;
            ngaytruoc = 30;
            thangsau = thang + 1;
            thangtruoc = thang;
            namsau = nam;
            namtruoc = nam;
        }
    }
    else if ((ngay == 30) && (thang == 4, 6, 9, 11)) {
        ngaysau = 1;
        ngaytruoc = 29;
        thangsau = thang + 1;
        thangtruoc = thang;
        namsau = nam;
        namtruoc = nam;
    } else if (((nam % 4 == 0) && (thang == 2) && (ngay == 29)) || ((thang == 2) && (nam % 4 != 0) && (ngay == 28))) {
        ngaysau = 1;
        ngaytruoc = ngay - 1;
        thangsau = 3;
        thangtruoc = 2;
        namsau = nam;
        namtruoc = nam;
    } else if ((ngay >= 2) && (ngay < 30)) {
        ngaytruoc = ngay - 1;
        ngaysau = ngay + 1;
        thangtruoc = thang;
        thangsau = thang;
        namtruoc = nam;
        namsau = nam;
    }


    document.getElementById('ngayTruocdo').innerHTML = ngaytruoc + '/' + thangtruoc + '/' + namtruoc;
    document.getElementById('ngaySaudo').innerHTML = ngaysau + '/' + thangsau + '/' + namsau;
}

/* 2. Viết chương trình nhập vào tháng, năm. Cho biết tháng đó có bao nhiêu ngày. (bao gồm tháng
    của năm nhuận). */

document.getElementById('Tinh2').onclick = function () {

    var thang = Number(document.getElementById('Thang2').value);
    var nam = Number(document.getElementById('Nam2').value);

    var ngay = 0

    if (thang == 2) {
        if (nam % 4 == 0) {
            ngay = 29;
        }
        else {
            ngay = 28;
        }
    } else if ((thang == 1) || (thang == 3) || (thang == 5) || (thang == 7) || (thang == 8) || (thang == 10) || (thang == 12)) {
        ngay = 31;
    } else {
        ngay = 30;
    }

    document.getElementById('ketQua2').innerHTML = ngay;

}

// 3. Viết chương trình nhập vào số nguyên có 3 chữ số. In ra cách đọc nó.

document.getElementById('Tinh3').onclick = function () {

    var socandoc = Number(document.getElementById('socandoc').value);


    var tram = Math.floor(socandoc / 100);
    var chuc = Math.floor((socandoc - (tram * 100)) / 10);
    var donvi = socandoc - tram * 100 - chuc * 10;

    function doc(a) {
        if (a == 1) {
            return 'Một';
        } else if (a == 2) {
            return 'Hai';
        } else if (a == 3) {
            return 'Ba';
        } else if (a == 4) {
            return 'Bốn';
        } else if (a == 5) {
            return 'Năm';
        } else if (a == 6) {
            return 'Sáu';
        } else if (a == 7) {
            return 'Bảy';
        } else if (a == 8) {
            return 'Tám';
        } else if (a == 9) {
            return 'Chín';
        }
    }

    if (socandoc > 999 || socandoc < 100) {
        alert('số nhập vào không đúng');
    } else if (chuc == 0 && donvi == 0) {
        document.getElementById('ketQua3').innerHTML = doc(tram) + ' trăm ';
    }
    else if (chuc == 0) {
        document.getElementById('ketQua3').innerHTML = doc(tram) + ' trăm ' + ' linh ' + doc(donvi);
    } else if (donvi == 0) {
        document.getElementById('ketQua3').innerHTML = doc(tram) + ' trăm ' + doc(chuc) + ' mươi ';
    }
    else {
        document.getElementById('ketQua3').innerHTML = doc(tram) + ' trăm ' + doc(chuc) + ' mươi ' + doc(donvi);
    }

}
/*    4. Cho biết tên và tọa độ nhà của 3 sinh viên. Cho biết tọa độ của trường đại học. Viết chương
    trình in tên sinh viên xa trường nhất. */

document.getElementById('Tinh4').onclick = function () {

    var x1 = Number(document.getElementById('toadox1').value);
    var y1 = Number(document.getElementById('toadoy1').value);

    var x2 = Number(document.getElementById('toadox2').value);
    var y2 = Number(document.getElementById('toadoy2').value);

    var x3 = Number(document.getElementById('toadox3').value);
    var y3 = Number(document.getElementById('toadoy3').value);

    var xtrg = Number(document.getElementById('toadoxtrg').value);
    var ytrg = Number(document.getElementById('toadoytrg').value);

    var khoangcach1 = Math.sqrt((x1 - xtrg) ** 2 + (y1 - ytrg) ** 2);
    var khoangcach2 = Math.sqrt((x2 - xtrg) ** 2 + (y2 - ytrg) ** 2);
    var khoangcach3 = Math.sqrt((x3 - xtrg) ** 2 + (y3 - ytrg) ** 2);

    if (khoangcach1 >= khoangcach2 && khoangcach1 >= khoangcach3) {
        document.getElementById('ketQua4').innerHTML
            = document.getElementById('hoten1').value;
    } else if (khoangcach2 >= khoangcach1 && khoangcach2 >= khoangcach3) {
        document.getElementById('ketQua4').innerHTML
            = document.getElementById('hoten2').value;
    } else if (khoangcach3 >= khoangcach2 && khoangcach3 >= khoangcach1) {
        document.getElementById('ketQua4').innerHTML
            = document.getElementById('hoten3').value;
    }
}
